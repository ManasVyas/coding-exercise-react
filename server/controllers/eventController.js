const pool = require("../config/database");

const getAllEvents = async (req, res, next) => {
  try {
    const data = await pool.query(
      `SELECT event_id AS "eventId", title, time, TO_CHAR(date :: DATE, 'dd-mm-yyyy') AS date FROM event ORDER BY time ASC`
    );
    res.status(200).json({ status: "success", data: data.rows });
  } catch (error) {
    next(error);
  }
};

const getEventById = async (req, res, next) => {
  try {
    const data = await pool.query(
      `SELECT event_id AS "eventId", title, time, TO_CHAR(date :: DATE, 'dd-mm-yyyy') AS date FROM event WHERE event_id = $1`,
      [req.params.id]
    );
    if (data.rows.length === 0) {
      const error = new Error("Event not found!");
      error.status = 404;
      return next(error);
    }
    res.status(200).json({ status: "success", data: data.rows });
  } catch (error) {
    next(error);
  }
};

const createEvent = async (req, res, next) => {
  try {
    const { title, time, date } = req.body;
    const data = await pool.query(
      `INSERT INTO event (title, time, date) VALUES ($1, $2, $3) RETURNING event_id AS "eventId", title, time, TO_CHAR(date :: DATE, 'dd-mm-yyyy') AS date`,
      [title, time, date]
    );
    res.status(201).json({ status: "success", data: data.rows });
  } catch (error) {
    next(error);
  }
};

const updateEvent = async (req, res, next) => {
  try {
    const { eventId, title, time, date } = req.body;
    const data = await pool.query(`SELECT * FROM event WHERE event_id = $1`, [
      eventId,
    ]);
    if (data.rows.length === 0) {
      const error = new Error("Event not found!");
      error.status = 404;
      return next(error);
    }
    const event = await pool.query(
      `UPDATE event SET title = $1, time = $2, date = $3 WHERE event_id = $4 RETURNING event_id AS "eventId", title, time, TO_CHAR(date :: DATE, 'dd-mm-yyyy') AS date`,
      [title, time, date, eventId]
    );
    res.status(200).json({ status: "success", data: event.rows });
  } catch (error) {
    next(error);
  }
};

const deleteEvent = async (req, res, next) => {
  try {
    const { id } = req.params;
    const data = await pool.query(`SELECT * FROM event WHERE event_id = $1`, [
      id,
    ]);
    if (data.rows.length === 0) {
      const error = new Error("Event not found!");
      error.status = 404;
      return next(error);
    }
    const event = await pool.query(
      `DELETE FROM event WHERE event_id = $1 RETURNING event_id AS "eventId", title, time, TO_CHAR(date :: DATE, 'dd-mm-yyyy') AS date`,
      [id]
    );
    res.status(200).json({ status: "success", data: event.rows });
  } catch (error) {
    next(error);
  }
};

const eventController = {
  getAllEvents,
  getEventById,
  createEvent,
  updateEvent,
  deleteEvent,
};

module.exports = eventController;
