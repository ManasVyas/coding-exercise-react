const router = require("express").Router();
const eventController = require("../controllers/eventController");
const {
  addEventSchema,
  updateEventSchema,
  validateEvent,
} = require("../validation/eventValidation");

router.get("/", eventController.getAllEvents);
router.get("/:id", eventController.getEventById);
router.post(
  "/add",
  validateEvent(addEventSchema, "add"),
  eventController.createEvent
);
router.put(
  "/update",
  validateEvent(updateEventSchema, "update"),
  eventController.updateEvent
);
router.delete("/delete/:id", eventController.deleteEvent);

module.exports = router;
