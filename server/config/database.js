const Pool = require("pg").Pool;
require("dotenv").config();

const pool = new Pool({
  user: process.env.UNAME,
  host: process.env.HOST,
  database: process.env.DATABASE,
  password: process.env.PASSWORD,
  port: process.env.PORT,
});

pool.query(
  `CREATE TABLE IF NOT EXISTS "event" ( event_id serial PRIMARY KEY, title VARCHAR(20) NOT NULL, time TIME NOT NULL, date DATE NOT NULL )`
);

module.exports = pool;
