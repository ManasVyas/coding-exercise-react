const yup = require("yup");

const addEventSchema = yup.object().shape({
  title: yup.string().required(),
  time: yup.string().required(),
  date: yup.date().required(),
});

const updateEventSchema = yup.object().shape({
  eventId: yup.number().required(),
  title: yup.string().required(),
  time: yup.string().required(),
  date: yup.date().required(),
});

const validateEvent = (schema, type) => {
  return async (req, res, next) => {
    try {
      let validatedBody = {};
      if (type === "add") {
        validatedBody = await schema.validate(req.body);
      } else {
        validatedBody = await schema.validate(req.body);
      }
      req.body = validatedBody;
      next();
    } catch (error) {
      error.status = 400;
      next(error);
    }
  };
};

const validation = {
  addEventSchema,
  updateEventSchema,
  validateEvent,
};

module.exports = validation;
