import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import "./App.css";
import { loadEvents, addEvent } from "./redux/actions/eventActions";
import CalendarHeader from "./components/CalendarHeader/CalendarHeader";
import Day from "./components/Day/Day";
import NewEventModal from "./components/NewEventModal/NewEventModal";

const App = (props) => {
  const [nav, setNav] = useState(0);
  const [days, setDays] = useState([]);
  const [dateDisplay, setDateDisplay] = useState("");
  const [clicked, setClicked] = useState();
  const [events, setEvents] = useState([]);

  const eventForDate = (date) => events.filter((e) => e.date === date);

  const loadEvents = async () => {
    try {
      const response = await props.loadEvents();
      if (response && response.data && response.data.data) {
        setEvents(response.data.data);
      }
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    loadEvents();
  }, []);

  useEffect(() => {
    const weekdays = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];

    const dt = new Date();

    if (nav !== 0) {
      dt.setMonth(new Date().getMonth() + nav);
    }

    const day = dt.getDate();
    const month = dt.getMonth();
    const year = dt.getFullYear();

    const firstDayOfMonth = new Date(year, month, 1);
    const daysInMonth = new Date(year, month + 1, 0).getDate();

    const dateString = firstDayOfMonth.toLocaleDateString("en-us", {
      weekday: "long",
      year: "numeric",
      month: "numeric",
      day: "numeric",
    });
    setDateDisplay(
      `${dt.toLocaleDateString("en-in", { month: "long" })} ${year}`
    );
    const paddingDays = weekdays.indexOf(dateString.split(", ")[0]);

    const daysArr = [];

    for (let i = 1; i <= paddingDays + daysInMonth; i++) {
      const dayString =
        ("0" + (i - paddingDays)).slice(-2) +
        "-" +
        ("0" + (month + 1)).slice(-2) +
        "-" +
        year;
      if (i > paddingDays) {
        daysArr.push({
          value: i - paddingDays,
          event: eventForDate(dayString),
          isCurrentDay: i - paddingDays === day && nav === 0,
          date: dayString,
        });
      } else {
        daysArr.push({
          value: "padding",
          event: null,
          isCurrentDay: false,
          date: "",
        });
      }
    }
    setDays(daysArr);
  }, [events, nav]);

  return (
    <>
      <div id="container">
        <CalendarHeader
          dateDisplay={dateDisplay}
          onNext={() => setNav(nav + 1)}
          onBack={() => setNav(nav - 1)}
        />
        <div id="weekdays">
          <div>Sunday</div>
          <div>Monday</div>
          <div>Tuesday</div>
          <div>Wednesday</div>
          <div>Thursday</div>
          <div>Friday</div>
          <div>Saturday</div>
        </div>
        <div id="calendar">
          {days.map((d, index) => (
            <Day
              key={index}
              day={d}
              onClick={() => {
                if (d.value !== "padding") {
                  setClicked(d.date);
                }
              }}
              setClicked={setClicked}
              events={events}
              loadEvents={loadEvents}
            />
          ))}
        </div>
      </div>
      {clicked && (
        <NewEventModal
          date={clicked.split("-").reverse().join("-")}
          onSave={async (event) => {
            const response = await props.addEvent(event);
            if (response.status === 201) {
              loadEvents();
            } else {
              alert("Something went wrong!!");
            }
            setClicked(null);
          }}
          onClose={() => setClicked(null)}
        />
      )}
    </>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadEvents: () => dispatch(loadEvents()),
    addEvent: (event) => dispatch(addEvent(event)),
  };
};

export default connect(null, mapDispatchToProps)(App);
