import React, { useState } from "react";
import { connect } from "react-redux";
import { updateEvent, deleteEvent } from "../../redux/actions/eventActions";
import ModifyEventModal from "../ModifyEventModal/ModifyEventModal";

const Day = ({
  day,
  onClick,
  setClicked,
  events,
  loadEvents,
  updateEvent,
  deleteEvent,
}) => {
  const [eventClicked, setEventClicked] = useState();
  const className = `day ${day.value === "padding" ? "padding" : ""} ${
    day.isCurrentDay ? "currentDay" : ""
  }`;

  const eventDetails = (id) => events.find((e) => e.eventId === id);

  const handleEventClick = (e) => {
    setClicked(null);
    setEventClicked(e.eventId);
  };

  const onUpdate = async (eventDetails) => {
    const response = await updateEvent(eventDetails);
    if (response.status === 200) {
      loadEvents();
    } else {
      alert("Something went wrong!!");
    }
    setEventClicked(null);
  };

  const onDelete = async (id) => {
    const response = await deleteEvent(id);
    if (response.status === 200) {
      loadEvents();
    } else {
      alert("Something went wrong!!");
    }
    setEventClicked(null);
  };

  return (
    <>
      <div className={className}>
        <div onClick={onClick}>{day.value === "padding" ? "" : day.value}</div>
        {day.event &&
          day.event.map((e) => (
            <div
              onClick={() => handleEventClick(e)}
              key={e.eventId}
              className="event"
            >
              <p>{`${e.title} at ${e.time}`}</p>
            </div>
          ))}
      </div>
      {eventClicked && (
        <ModifyEventModal
          eventDetails={eventDetails(eventClicked)}
          onClose={() => setEventClicked(null)}
          onDelete={(id) => onDelete(id)}
          onUpdate={(eventDetail) => onUpdate(eventDetail)}
        />
      )}
    </>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateEvent: (eventDetails) => dispatch(updateEvent(eventDetails)),
    deleteEvent: (id) => dispatch(deleteEvent(id)),
  };
};

export default connect(null, mapDispatchToProps)(Day);
