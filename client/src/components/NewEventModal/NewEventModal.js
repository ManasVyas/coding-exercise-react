import React, { useState } from "react";

const NewEventModal = ({ onSave, onClose, date }) => {
  const [event, setEvent] = useState({
    title: "",
    time: "",
    date,
  });
  const [error, setError] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setEvent((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const onSaveClick = () => {
    if (event.title && event.time) {
      setError(false);
      onSave(event);
    } else {
      setError(true);
    }
  };

  return (
    <div>
      <div id="newEventModal">
        <h2>New Event</h2>

        <input
          className={error ? "error" : ""}
          value={event.title}
          name="title"
          onChange={handleChange}
          id="eventTitleInput"
          placeholder="Event Title"
          maxLength="20"
          required
        />
        <input
          type="time"
          className={error ? "error" : ""}
          name="time"
          value={event.time}
          onChange={handleChange}
          id="eventTitleInput"
          required
        />
        <input type="date" value={event.date} id="eventTitleInput" disabled />

        <button onClick={onSaveClick} id="saveButton">
          Save
        </button>

        <button onClick={onClose} id="cancelButton">
          Cancel
        </button>
      </div>

      <div id="modalBackDrop"></div>
    </div>
  );
};

export default NewEventModal;
