import React, { useState } from "react";

const ModifyEventModal = ({ onUpdate, onDelete, eventDetails, onClose }) => {
  const [event, setEvent] = useState(eventDetails);
  const [error, setError] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setEvent((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const onUpdateClick = () => {
    if (event.title && event.time) {
      event.date = event.date.split("-").reverse().join("-");
      setError(false);
      onUpdate(event);
    } else {
      setError(true);
    }
  };

  return (
    <>
      <div id="deleteEventModal">
        <h2>Modify Event</h2>
        <input
          className={error ? "error" : ""}
          value={event.title}
          name="title"
          onChange={handleChange}
          id="eventTitleInput"
          placeholder="Event Title"
          maxLength="20"
          required
        />
        <input
          type="time"
          className={error ? "error" : ""}
          name="time"
          value={event.time}
          onChange={handleChange}
          id="eventTitleInput"
          required
        />
        <input
          type="date"
          value={event.date.split("-").reverse().join("-")}
          id="eventTitleInput"
          disabled
        />
        <button onClick={onUpdateClick} id="updateButton">
          Update
        </button>
        <button onClick={() => onDelete(event.eventId)} id="deleteButton">
          Delete
        </button>
        <button onClick={onClose} id="closeButton">
          Close
        </button>
      </div>

      <div id="modalBackDrop"></div>
    </>
  );
};

export default ModifyEventModal;
