import * as types from "../actions/actionTypes";
import initialState from "./initialState";

const eventReducer = (state = initialState, action) => {
  const newState = { ...state };
  switch (action.type) {
    case types.LOAD_EVENTS:
      newState.events = action.events.data.data;
      return newState;
    default:
      return newState;
  }
};

export default eventReducer;
