import axios from "axios";
import config from "../../config.json";

export const loadEvents = async () => {
  try {
    const response = await axios.get(`${config.Backend_URL}event`);
    return response;
  } catch (error) {
    return error;
  }
};

export const addEvent = async (eventDetails) => {
  try {
    const response = await axios.post(
      `${config.Backend_URL}event/add`,
      eventDetails
    );
    return response;
  } catch (error) {
    return error;
  }
};

export const updateEvent = async (eventDetails) => {
  try {
    const response = await axios.put(
      `${config.Backend_URL}event/update`,
      eventDetails
    );
    return response;
  } catch (error) {
    return error;
  }
};

export const deleteEvent = async (id) => {
  try {
    const response = await axios.delete(
      `${config.Backend_URL}event/delete/${id}`
    );
    return response;
  } catch (error) {
    return error;
  }
};

export const loadEventById = async (id) => {
  try {
    const response = await axios.get(`${config.Backend_URL}event/${id}`);
    return response;
  } catch (error) {
    return error;
  }
};
