export const LOAD_EVENTS = "LOAD_EVENTS";
export const LOAD_EVENT_BY_ID = "LOAD_EVENT_BY_ID";
export const ADD_EVENT = "ADD_EVENT";
export const UPDATE_EVENT = "UPDATE_EVENT";
export const DELETE_EVENT = "DELETE_EVENT";
