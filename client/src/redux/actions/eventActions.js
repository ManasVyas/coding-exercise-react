import * as types from "./actionTypes";
import * as eventApi from "../apis/eventApis";

export const loadEventsSuccess = (events) => {
  return {
    type: types.LOAD_EVENTS,
    events,
  };
};

export const addEventSuccess = (event) => {
  return {
    type: types.ADD_EVENT,
    event,
  };
};

export const updateEventSuccess = (event) => {
  return {
    type: types.UPDATE_EVENT,
    event,
  };
};

export const deleteEventSuccess = (id) => {
  return {
    type: types.DELETE_EVENT,
    id,
  };
};

export const loadEventByIdSuccess = (id) => {
  return {
    type: types.LOAD_EVENT_BY_ID,
    id,
  };
};

export const loadEvents = () => {
  return async (dispatch) => {
    try {
      const events = await eventApi.loadEvents();
      dispatch(loadEventsSuccess(events));
      return events;
    } catch (error) {
      return error;
    }
  };
};

export const addEvent = (eventDetails) => {
  return async (dispatch) => {
    try {
      const event = await eventApi.addEvent(eventDetails);
      dispatch(addEventSuccess(event));
      return event;
    } catch (error) {
      return error;
    }
  };
};

export const updateEvent = (eventDetails) => {
  return async (dispatch) => {
    try {
      const event = await eventApi.updateEvent(eventDetails);
      dispatch(updateEventSuccess(event));
      return event;
    } catch (error) {
      return error;
    }
  };
};

export const deleteEvent = (id) => {
  return async (dispatch) => {
    try {
      const event = await eventApi.deleteEvent(id);
      dispatch(deleteEventSuccess(event));
      return event;
    } catch (error) {
      return error;
    }
  };
};

export const loadEventById = (id) => {
  return async (dispatch) => {
    try {
      const event = await eventApi.loadEventById(id);
      dispatch(loadEventByIdSuccess(id));
      return event;
    } catch (error) {
      return error;
    }
  };
};
